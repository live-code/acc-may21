import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Show, TvmazeResponse } from '../../model/tvmaze-response';

@Component({
  selector: 'app-tvmaze',
  template: `
    <form #f="ngForm" (submit)="search(f)" class="mb-4">
      <input 
        placeholder="Search TV Series"
        type="text" [ngModel] name="text" 
        class="form-control form-control-lg"
      >
    </form>
    
    <app-no-results *ngIf="!result?.length"></app-no-results>

    <div class="row">
      <div
        style="text-align: center; min-width: 200px; margin-bottom: 10px"
        class="col" 
        *ngFor="let series of result"
        (click)="showSeries(series)"
      >
        <div>{{series.show.name}}</div>
        <img [src]="series.show.image?.medium" alt="">
        <img *ngIf="!series.show.image" 
             src="assets/angop112.png" alt="">
      </div>
   </div>
    
    <div *ngIf="selectedSeries" class="my-modal">
      
      <i class="fa fa-times fa-2x close-button" (click)="selectedSeries = null"></i>
      <img [src]="selectedSeries.image?.original" width="100%">
   
      <div class="my-modal-content">
        <h3>{{selectedSeries.name}}</h3>
        <a [href]="selectedSeries.url" target="_blank">Visit TVMAze</a>
        
        <div [innerHTML]="selectedSeries.summary"></div>
        
        {{selectedSeries.genres}}
      </div>
    </div>
    
    <pre>{{selectedSeries | json}}</pre>
  `,
  styles: [`
    .my-modal {
      position: fixed;
      top: 0; right: 0; left: 0; bottom: 0;
      background-color: black;
      overflow-y: auto;
      color: white;
    }
    
    .my-modal-content {
      margin: 20px;
    }
    
    .close-button {
      position: fixed;
      top: 30px;
      right: 30px;
    }
  `]
})
export class TvmazeComponent {
  result: TvmazeResponse[];
  selectedSeries: Show;

  constructor(private http: HttpClient) { }

  search(f: NgForm): void {
    this.http.get<TvmazeResponse[]>('http://api.tvmaze.com/search/shows?q=' + f.value.text)
      .subscribe(res => {
        this.result = res;
      });
  }

  showSeries(series: TvmazeResponse): void {
    this.selectedSeries = series.show;
  }

}
