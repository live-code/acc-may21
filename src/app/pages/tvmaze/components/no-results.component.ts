import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-no-results',
  template: `
    <div class="text-center">
      <span style="background-color: #dedede; padding: 20px; border-radius: 20px">
        <i class="fa fa-times"></i>
        No results
      </span>
    </div>
  `,
})
export class NoResultsComponent {


}
