import { Component } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { debounceTime, filter, map } from 'rxjs/operators';

@Component({
  selector: 'app-meteo',
  template: `
    <br>
    <mat-tab-group [selectedIndex]="value">
      <mat-tab label="Other">
        Click on meteo
      </mat-tab>
      
      <mat-tab label="Meteo">
        <input type="text" [formControl]="input">
        <app-weather [city]="city"></app-weather>  
      </mat-tab>
     
    </mat-tab-group>
  `,
})
export class MeteoComponent {
  value = 0;
  city = 'London';
  input = new FormControl('rome', Validators.minLength(3));

  constructor() {
    this.input.valueChanges
      .pipe(
        map(text => text.toLowerCase()),
        filter(text => text.length > 2),
        debounceTime(1000)
      )
      .subscribe(text => this.city = text);
  }
}
