import { Component, OnInit } from '@angular/core';
import { City, Country } from '../model/country';
import { count } from 'rxjs/operators';

@Component({
  selector: 'app-demo-tabbar',
  template: `
    <app-tabbar 
      [items]="countries"
      [active]="activeCountry"
      (tabClick)="selectCountryHandler($event)"
    ></app-tabbar>
    
    <app-tabbar
      *ngIf="activeCountry"
      [items]="activeCountry.cities"
      [active]="activeCity"
      (tabClick)="selectCityHandler($event)"
    ></app-tabbar>
    
    <div *ngIf="activeCity">
      <img
        [src]="'https://maps.googleapis.com/maps/api/staticmap?center=' + activeCity.name + '&zoom=5&size=200x100&key=AIzaSyDSBmiSWV4-AfzaTPNSJhu-awtfBNi9o2k'" alt="">
    </div>
  `,
})
export class DemoTabbarComponent {
  countries: Country[];
  activeCountry: Country;
  activeCity: City;

  constructor() {
    setTimeout(() => {
      this.countries = [
        {
          id: 1,
          name: 'Italy',
          cities: [ { id: 10, name: 'Rome'}, { id: 11, name: 'Milan'}]
        },
        {
          id: 2,
          name: 'Germany',
          cities: [ { id: 12, name: 'Berlin'}]
        },
        {
          id: 3,
          name: 'Spain',
          cities: [ { id: 13, name: 'Madrid'}, { id: 14, name: 'Barcelona'}]
        },
      ];
      this.selectCountryHandler(this.countries[0])
    }, 1000);
  }

  selectCountryHandler(country: Country): void {
    this.activeCountry = country;
    this.activeCity = country.cities[0];
  }

  selectCityHandler(city: City): void {
    this.activeCity = city;
  }


}
