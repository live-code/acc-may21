import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  template: `
    <app-hello
      color="red"
      title="pippo" 
      icon="fa fa-times"
    ></app-hello>

    <app-card 
      title="Profilo" 
      icon="fa fa-user"
      footerLabel="GO TO NEXT STEP"
      *ngIf="step === 0"
      (footerButtonClick)="step = 1"
    >
      <input type="text" class="form-control">
      <input type="text" class="form-control">
      <input type="text" class="form-control">
    </app-card>
    
    
   
    <br>
    <app-card 
      title="TV Series" 
      icon="fa fa-link"
      footerLabel="SUBMIT"
      (iconClick)="openUrl('http://www.google.com')"
      *ngIf="step === 1"
      (footerButtonClick)="doSomething()"

    >
      <div class="row">
        <div class="col">
          <app-card title="left">....</app-card>
        </div>
        <div class="col">
          <app-card title="right">....</app-card>
        </div>
      </div>
    </app-card>
  `,
  styles: [
  ]
})
export class HomeComponent {
  step = 0;

  openUrl(url: string): void {
    window.open(url)
  }

  doSomething(): void {
    console.log('DO!')
  }
}
