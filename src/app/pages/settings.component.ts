import { Component, OnInit } from '@angular/core';
import { ThemeService } from '../core/services/theme.service';

@Component({
  selector: 'app-settings',
  template: `
   
    <h4>Theme Settings {{themeService.value}}</h4>
    <button (click)="themeService.setTheme('dark')">Dark</button>
    <button (click)="themeService.setTheme('light')">light</button>
  `,
  styles: [
  ]
})
export class SettingsComponent implements OnInit {
  constructor(public themeService: ThemeService) {}

  ngOnInit(): void {
  }

}
