import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Device } from '../../../model/device';

@Injectable({ providedIn: 'root' })
export class DeviceDetailsService {
  data: Partial<Device> ;
  id: number;

  constructor(private http: HttpClient) { }

  getDeviceById(id: number): void{
    this.id = id;
    this.http.get<Device>(`http://localhost:3000/devices/${id}`)
      .subscribe(res => {
        this.data = res;
      });
  }

  editItem(user: Partial<Device>, id: number): void {
    this.http.patch<Partial<Device>>(`http://localhost:3000/devices/${this.id}`, user)
      .subscribe(res => {
        this.data = res;
      });
  }


}
