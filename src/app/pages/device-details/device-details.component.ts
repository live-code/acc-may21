import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DeviceDetailsService } from './services/device-details.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-device-details',
  template: `
   <h1>Device Details</h1>
   
   <div *ngIf="!deviceDetailsService.data">loading....</div>
   
   <form #f="ngForm" (submit)="deviceDetailsService.editItem(f.value, id)">
     <input type="text" name="name" [ngModel]="deviceDetailsService.data?.name">
     <input type="text" name="cost" [ngModel]="deviceDetailsService.data?.cost">
     <button type="submit">UPDATE</button>
   </form>
   <pre>{{deviceDetailsService.data | json}}</pre>
  `,
})
export class DeviceDetailsComponent implements OnInit  {
  id: number;

  constructor(
    private activatedRoute: ActivatedRoute,
    public deviceDetailsService: DeviceDetailsService
  ) {}

  ngOnInit(): void {
    this.id = this.activatedRoute.snapshot.params.id;
    this.deviceDetailsService.getDeviceById(this.id);
  }

}
