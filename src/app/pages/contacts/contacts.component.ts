import { Component, OnInit } from '@angular/core';
import { ThemeService } from '../../core/services/theme.service';

@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.css']
})
export class ContactsComponent implements OnInit {
  label = 'pippo';

  constructor(private themeService: ThemeService) {
    console.log(themeService.value)

    setTimeout(() => {
      this.label = 'ciccio'
    }, 3000)
  }

  ngOnInit(): void {
  }

}
