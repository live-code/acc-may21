import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { News, Welcome } from './model/welcome';


@Component({
  selector: 'app-welcome',
  template: `
    <mat-tab-group >

      <mat-tab label="Editor">
        <ng-template matTabContent>
         <app-welcome-editor></app-welcome-editor>
        </ng-template>
      </mat-tab>
      
      <mat-tab label="Preview">
        <ng-template matTabContent>
          <app-welcome-preview></app-welcome-preview>
        </ng-template>
      </mat-tab>


    </mat-tab-group>
    
    
  `,
  styles: [
  ]
})
export class WelcomeComponent implements OnInit {

  ngOnInit(): void {
  }

}
