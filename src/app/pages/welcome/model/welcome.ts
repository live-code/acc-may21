
export interface Hero  {
  title: string;
  desc: string;
  subdesc: string;
  buttonLabel: string;
  buttonUrl: string;
}

export interface Welcome {
  hero: Hero;
  footer: {
    bg: string;
    row1: string;
    row2: string;
    email: string;
  };
}

export interface News {
  id: number;
  title: string;
  img: string;
}
