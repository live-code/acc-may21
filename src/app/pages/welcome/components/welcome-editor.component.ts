import { Component, OnInit } from '@angular/core';
import { Hero, News, Welcome } from '../model/welcome';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-welcome-editor',
  template: `
    <form #f="ngForm" (submit)="saveHero(f.value)">
      <div class="jumbotron">
        <h1 class="display-4">
          <input type="text" placeholder="titolo" [ngModel]="welcome?.hero.title" name="title">
        </h1>
        <p class="lead">
          <textarea  [ngModel]="welcome?.hero.desc" name="desc" cols="30" rows="5" placeholder="Description"> </textarea>
        </p>
        <hr class="my-4">
        <p>{{welcome?.hero.subdesc}}</p>
        <a class="btn btn-primary btn-lg" [href]="welcome?.hero.buttonUrl" role="button">
          {{welcome?.hero.buttonLabel}}
        </a>
      </div>
      
      <button type="submit" hidden>save</button>
    </form>

    <div class="row">
      <div
        *ngFor="let n of news"
        class="col-sm-12 col-md-4 pb-2"
      >
        <app-card [title]="n.title"
                  icon="fa fa-trash"
                  (iconClick)="deleteNews(n)"
        >
          <img [src]="n.img" width="100%">
        </app-card>
      </div>
    </div>

    <div class="fixed-bottom text-center p-3" [style.background-color]="welcome?.footer.bg">
      {{welcome?.footer.row1}} <br>
      {{welcome?.footer.row2}} - {{welcome?.footer.email}}<br>
    </div>

  `,
  styles: [
  ]
})
export class WelcomeEditorComponent {
  welcome: Welcome;
  news: News[];

  constructor(private http: HttpClient) {
    http.get<Welcome>('http://localhost:3000/welcome')
      .subscribe(res => {
        this.welcome = res;
      });

    http.get<News[]>('http://localhost:3000/news')
      .subscribe(res => {
        this.news = res;
      });
  }

  deleteNews(news: News): void {
    console.log(news)
    this.http.delete(`http://localhost:3000/news/${news.id}`)
      .subscribe(() => {
        const index = this.news.findIndex(n => n.id === news.id);
        this.news.splice(index, 1)
      })
  }

  saveHero(data: Hero): void {

    this.http.patch('http://localhost:3000/welcome', {
      hero: { ...this.welcome.hero, ...data }
    })
      .subscribe()

  }
}
