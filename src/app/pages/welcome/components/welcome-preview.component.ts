import { Component, OnInit } from '@angular/core';
import { News, Welcome } from '../model/welcome';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-welcome-preview',
  template: `
    <div class="jumbotron">
      <h1 class="display-4">{{welcome?.hero.title}}</h1>
      <p class="lead">{{welcome?.hero.desc}}</p>
      <hr class="my-4">
      <p>{{welcome?.hero.subdesc}}</p>
      <a class="btn btn-primary btn-lg" [href]="welcome?.hero.buttonUrl" role="button">
        {{welcome?.hero.buttonLabel}}
      </a>
    </div>


    <div class="row">
      <div
        *ngFor="let n of news"
        class="col-sm-12 col-md-4 pb-2"
      >
        <app-card [title]="n.title">
          <img [src]="n.img" width="100%">
        </app-card>
      </div>
    </div>

    <div class="fixed-bottom text-center p-3" [style.background-color]="welcome?.footer.bg">
      {{welcome?.footer.row1}} <br>
      {{welcome?.footer.row2}} - {{welcome?.footer.email}}<br>
    </div>

  `,
  styles: [
  ]
})
export class WelcomePreviewComponent  {

  welcome: Welcome;
  news: News[];

  constructor(private http: HttpClient) {
    http.get<Welcome>('http://localhost:3000/welcome')
      .subscribe(res => {
        this.welcome = res;
      });

    http.get<News[]>('http://localhost:3000/news')
      .subscribe(res => {
        this.news = res;
      });


  }


}
