import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { PrivacyDialogComponent } from './components/privacy-dialog.component';

@Component({
  selector: 'app-demo-material',
  template: `
    
    <div class="container text-center">
      <form #f="ngForm" (submit)="save(f.value)">
        <mat-form-field>
          <mat-label>User name</mat-label>
          <input 
            matInput placeholder="Ex. John"
            ngModel
            name="name"
            required
          >
        </mat-form-field>
        
        <br>
  
        <mat-form-field appearance="fill">
          <mat-label>BirthDay</mat-label>
          <input matInput ngModel name="birthday" [matDatepicker]="picker">
          <mat-datepicker-toggle matSuffix [for]="picker"></mat-datepicker-toggle>
          <mat-datepicker  #picker></mat-datepicker>
        </mat-form-field>
        
        <div>
          <mat-slider 
            name="age"
            [ngModel]="value" min="1" max="100" step="1" value="1"
          ></mat-slider>
        </div>

        <button type="button" mat-button (click)="openDialog()" color="accent">Open privacy</button>
        
        <div>
          <button
            [disabled]="!privacyConfirmed || f.invalid"
            type="submit" mat-flat-button color="primary">SAVE</button>
        </div>
      </form>
    </div>
  `,
  styles: [
  ]
})
export class DemoMaterialComponent {
  value = 50;
  privacyConfirmed = false;

  constructor(public dialog: MatDialog) {}

  openDialog(): void {
    const dialogRef = this.dialog.open(PrivacyDialogComponent);

    dialogRef.afterClosed().subscribe(result => {
      this.privacyConfirmed = result;
      console.log(`Dialog result: ${result}`);
    });
  }

  save(formData: any): void {
    console.log(formData)
  }
}
