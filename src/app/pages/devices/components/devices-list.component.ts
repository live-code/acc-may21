import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { DevicesService } from '../services/devices.service';
import { Device } from '../../../model/device';

@Component({
  selector: 'app-devices-list',
  template: `
    <div
      *ngFor="let device of devices; let i = index; let last = last"
     
      [style.border-bottom]="last ? '1px solid black' : null"
    >
      <div class="card mb-3" [ngClass]="{ high: device.cost > 1000}">
        <div class="card-header" [ngClass]="{ 'bg-dark text-white': device.id === selected?.id }">

          <i
            class="fa"
            [ngClass]="{
                'fa-android': device.brand === 'android',
                'fa-apple': device.brand === 'apple'
              }"
            [style.color]="device.brand === 'apple' ? 'grey' : 'lightgreen'"
          ></i>

          {{device.name}}

          <div class="pull-right">
         <!--   <i class="fa fa-info-circle fa-2x" [routerLink]="device.id" ></i>
            <i class="fa fa-trash fa-2x"
               (click)="deleteHandler(device, $event)"></i>
-->
            <button mat-icon-button [matMenuTriggerFor]="menu" aria-label="Example icon-button with a menu">
              <mat-icon>more_vert</mat-icon>
            </button>
            <mat-menu #menu="matMenu">
              <button mat-menu-item (click)="deleteHandler(device, $event)">
                <mat-icon>delete</mat-icon>
                <span>Delete Item</span>
              </button>
              <button mat-menu-item [routerLink]="device.id" >
                <mat-icon>list</mat-icon>
                <span>Go to Details</span>
              </button>
              <button mat-menu-item  (click)="selectDevice.emit(device)">
                <mat-icon>done</mat-icon>
                <span>Select</span>
              </button>
            </mat-menu>
          </div>
          
          
        </div>

        <!--DEVICE DESCRIPTION-->
        <div class="card-body">
          <div
            *ngIf="device.cost"
            [class.text-high]="device.cost > 1000"
          >
            € {{device.cost | number: '1.2-4'}}

            <small *ngIf="device.cover" class="badge badge-dark">Cover included</small>
          </div>


          <div *ngIf="device.ram">Memory: {{device.ram / 1000}} Gb</div>

          <div>
            {{device.creationDate | date: 'dd/MM/yyyy @ hh:mm:ss'}}
          </div>

          <!--GOOGLE MAP-->
          <div *ngIf="device.country">
            <img
              width="200"
              [src]="'https://maps.googleapis.com/maps/api/staticmap?center=' + device.country + '&zoom=' + zoom + '&size=500x100&key=AIzaSyDSBmiSWV4-AfzaTPNSJhu-awtfBNi9o2k'" alt="">
            <div>
              <button (click)="zoom = zoom - 1">-</button>
              <button (click)="zoom = zoom + 1">+</button>
            </div>
          </div>

        </div>
      </div>

    </div>
  `,
  styles: [
  ]
})
export class DevicesListComponent {
  @Input() devices: Partial<Device>[];
  @Input() selected: Partial<Device>;
  @Output() selectDevice: EventEmitter<Partial<Device>> = new EventEmitter<Partial<Device>>();
  @Output() deleteDevice: EventEmitter<Partial<Device>> = new EventEmitter<Partial<Device>>();
  zoom = 5;

  deleteHandler(device: Partial<Device>, event: MouseEvent): void {
    event.stopPropagation();
    this.deleteDevice.emit(device);
  }
}
