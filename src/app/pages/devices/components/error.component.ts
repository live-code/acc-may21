import { Component, OnInit } from '@angular/core';
import { DevicesService } from '../services/devices.service';

@Component({
  selector: 'app-error',
  template: `
    <div class="alert alert-danger">
      errore lato server
    </div>
  `,
})
export class ErrorComponent {

}
