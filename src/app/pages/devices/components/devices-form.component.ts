import { Component, EventEmitter, Input, OnChanges, OnInit, Output, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Device } from '../../../model/device';

@Component({
  selector: 'app-devices-form',
  template: `

    <form
      #f="ngForm"
      (submit)="save.emit(f.value)"
      style="border: 5px solid white; padding: 10px"
      [style.border-color]="f.invalid && f.touched ? 'red' : 'rgba(0,0,0,0)'"
    >
      <input
        placeholder="name"
        class="form-control"
        type="text"
        [ngClass]="{ 'is-invalid': inputName.invalid && f.dirty, 'is-valid': inputName.valid }"
        #inputName="ngModel"
        [ngModel]="selected?.name"
        name="name"
        required
      >

      <div *ngIf="inputRam.errors?.required && f.dirty">Il campo è obbligatorio</div>
      <div *ngIf="inputRam.errors?.minlength && f.dirty">
        Il campo deve contenere almeno {{inputRam.errors?.minlength.requiredLength - inputRam.errors?.minlength.actualLength}} caratteri
      </div>
      <input
        placeholder="ram (in mb)" class="form-control"
        type="text"
        [ngModel]="selected?.ram"
        #inputRam="ngModel"
        name="ram"
        required minlength="4"
        [ngClass]="{ 'is-invalid': inputRam.invalid && f.dirty, 'is-valid': inputRam.valid }"
      >
      <input
        placeholder="cost" class="form-control"
        type="number"
        [ngModel]="selected?.cost"
        name="cost">

      <div class="btn-group mt-3">
        <button type="submit" class="btn btn-primary" [disabled]="f.invalid">
          <i class="fa fa-save"></i>
          {{selected?.id ? 'EDIT' : 'ADD'}}
        </button>

        <button
          type="button"
          class="btn btn-outline-primary"
          (click)="clean.emit()"
        >CLEAN</button>
        <!---->
      </div>
    </form>

  `,
  styles: [
  ]
})
export class DevicesFormComponent implements OnChanges {
  @Output() save: EventEmitter<Partial<Device>> = new EventEmitter();
  @Output() clean: EventEmitter<void> = new EventEmitter();
  @Input() selected: Partial<Device>;
  @ViewChild('f', { static: true }) form: NgForm;

  ngOnChanges(): void {
    if (!this.selected?.id) {
      this.form.reset();
    }
  }
}
