import { Component, OnDestroy } from '@angular/core';
import { Device } from '../../model/device';
import { DevicesService } from './services/devices.service';

@Component({
  selector: 'app-devices',
  template: `
    <app-error *ngIf="devicesService.error"></app-error>

    <app-devices-form
      [selected]="devicesService.selectedDevice"
      (save)="saveHandler($event)"
      (clean)="devicesService.clean()"
    ></app-devices-form>
    
    <div class="container mt-3">
      <app-devices-list 
        [devices]="devicesService.devices"
        [selected]="devicesService.selectedDevice"
        (selectDevice)="devicesService.setSelectedDevice($event)"
        (deleteDevice)="devicesService.deleteItem($event)"
      ></app-devices-list>
    </div>
  `,
  providers: [DevicesService ],
})
export class DevicesComponent implements OnDestroy {
  constructor(public devicesService: DevicesService) {
    devicesService.loadDevices();
  }

  saveHandler(device: Partial<Device>): void {
    this.devicesService.saveItem(device)
      .then(() => console.log('any action'))
      .catch(err => console.log('errore', err));
  }

  ngOnDestroy(): void {
    this.devicesService.clean();
  }

}

