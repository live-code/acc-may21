import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Device } from '../../../model/device';

@Injectable()
export class DevicesService {
  devices: Partial<Device>[] = [];
  selectedDevice: Partial<Device>;
  error = false;

  constructor(private http: HttpClient) {}

  loadDevices(): void {
    this.http.get<Device[]>('http://localhost:3000/devices')
      .subscribe(
        (res) => {
          this.devices = res;
        },
        (err: HttpErrorResponse) => {
          this.error = true;
        }
      );
  }


  deleteItem(device: Partial<Device>): void {
    this.error = false;
    this.http.delete<void>(`http://localhost:3000/devices/${device.id}`)
      .subscribe(
        () => {
          const index = this.devices.findIndex(d => d.id === device.id);
          this.devices.splice(index, 1);
        },
        (err: HttpErrorResponse) => {
          this.error = true;
        }
      );
  }
  saveItem(device: Partial<Device>): Promise<Partial<Device>> {
    if (this.selectedDevice?.id) {
      this.editItem(device);
    } else {
       return this.addItem(device)
    }
  }

  editItem(device: Partial<Device>): void {
    this.http.patch<Partial<Device>>(`http://localhost:3000/devices/${this.selectedDevice.id}`, device)
      .subscribe(res => {
        const index = this.devices.findIndex(d => d.id === this.selectedDevice.id);
        this.devices[index] = res;
      });
  }


  addItem(device: Partial<Device>): Promise<Partial<Device>> {
    return new Promise((resolve, reject) => {
      this.error = false;
      this.http.post<Partial<Device>>('http://localhost:3000/devices', device)
        .subscribe(
          res => {
            this.devices.push(res);
            this.selectedDevice = {};
            resolve(res);
          },
          (err) => {
            this.error = true;
            reject(err);
          }
        );
    });
  }

  setSelectedDevice(device: Partial<Device>): void {
    this.selectedDevice = device;
  }

  clean(): void {
    this.selectedDevice = {};
  }

}
