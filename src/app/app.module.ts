import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { DevicesComponent } from './pages/devices/devices.component';
import { TvmazeComponent } from './pages/tvmaze/tvmaze.component';
import { NoResultsComponent } from './pages/tvmaze/components/no-results.component';
import { RouterModule } from '@angular/router';
import { ContactsComponent } from './pages/contacts/contacts.component';
import { SettingsComponent } from './pages/settings.component';
import { DeviceDetailsComponent } from './pages/device-details/device-details.component';
import { DevicesService } from './pages/devices/services/devices.service';
import { HelloComponent } from './shared/components/hello.component';
import { HomeComponent } from './pages/home/home.component';
import { CardComponent } from './shared/components/card.component';
import { DemoTabbarComponent } from './pages/demo-tabbar.component';
import { TabbarComponent } from './shared/components/tabbar.component';
import { ErrorComponent } from './pages/devices/components/error.component';
import { DevicesFormComponent } from './pages/devices/components/devices-form.component';
import { DevicesListComponent } from './pages/devices/components/devices-list.component';
import { MeteoComponent } from './pages/meteo/meteo.component';
import { WeatherComponent } from './shared/components/weather.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DemoMaterialComponent } from './pages/demo-material/demo-material.component';
import { MatSliderModule } from '@angular/material/slider';
import { MatInputModule } from '@angular/material/input';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatButtonModule } from '@angular/material/button';
import { MatMenuModule } from '@angular/material/menu';
import { MatIconModule } from '@angular/material/icon';
import { MatTabsModule } from '@angular/material/tabs';
import { MatDialogModule } from '@angular/material/dialog';
import { PrivacyDialogComponent } from './pages/demo-material/components/privacy-dialog.component';
import { WelcomeComponent } from './pages/welcome/welcome.component';
import { WelcomePreviewComponent } from './pages/welcome/components/welcome-preview.component';
import { WelcomeEditorComponent } from './pages/welcome/components/welcome-editor.component';
import { APP_BASE_HREF } from '@angular/common';

@NgModule({
  declarations: [
    AppComponent,
    DevicesComponent,

    // tv maze
    TvmazeComponent,
    NoResultsComponent,

    ContactsComponent,
    SettingsComponent,
    DeviceDetailsComponent,
    HomeComponent,

    // Shared
    HelloComponent,
    CardComponent,
    DemoTabbarComponent,
    TabbarComponent,
    ErrorComponent,
    DevicesFormComponent,
    DevicesListComponent,
    MeteoComponent,
    WeatherComponent,
    DemoMaterialComponent,
    PrivacyDialogComponent,
    WelcomeComponent,
    WelcomePreviewComponent,
    WelcomeEditorComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule.forRoot([
      { path: 'home', component: HomeComponent},
      { path: 'welcome', component: WelcomeComponent},
      { path: 'tvmaze', component: TvmazeComponent},
      { path: 'material', component: DemoMaterialComponent},
      { path: 'meteo', component: MeteoComponent},
      { path: 'devices', component: DevicesComponent},
      { path: 'devices/:id', component: DeviceDetailsComponent},
      { path: 'demo-tabbar', component: DemoTabbarComponent},
      { path: 'contacts', component: ContactsComponent},
      { path: 'settings', component: SettingsComponent},
      { path: '',  redirectTo: 'home', pathMatch: 'full'},
      { path: '**', redirectTo: 'tvmaze'}
    ]),
    BrowserAnimationsModule,
    MatSliderModule,
    MatInputModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatButtonModule,
    MatMenuModule,
    MatTabsModule,
    MatIconModule,
    MatDialogModule
  ],
  providers: [
    // {provide: APP_BASE_HREF, useValue: '/my/app'}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
