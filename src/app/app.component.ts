import { Component } from '@angular/core';
import { ThemeService } from './core/services/theme.service';

@Component({
  selector: 'app-root',
  template: `
    
    <div
      style="padding: 10px"
      [style.background-color]="themeService.value === 'dark' ? '#222' : '#dedede'"
    >
    <button routerLink="welcome">welcome</button>
    <button routerLink="tvmaze">tvmaze</button>
    <button routerLink="devices">devices</button>
    <button routerLink="material">material</button>
    <button routerLink="settings">settings</button>
    <button routerLink="contacts">contacts</button>
    <button routerLink="demo-tabbar">demo-tabbar</button>
    <button routerLink="meteo">meteo</button>
    
    </div>
    <hr>

    <div class="container">
      <router-outlet></router-outlet>
    </div>



  `,
})
export class AppComponent {

  constructor(public themeService: ThemeService) { }
}

