export interface Device {
  id: number;
  name: string;
  brand: string;
  ram: number;
  cost: number;
  creationDate: number;
  country: string;
  cover?: boolean;
}
