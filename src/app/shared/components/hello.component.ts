import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-hello',
  template: `
    <h1 [style.color]="color">
      <i [class]="icon"></i>
      Hello {{title}}
    </h1>
  `
})
export class HelloComponent {
  @Input() title;
  @Input() icon = 'fa fa-heart';
  @Input() color = '#000';
}
