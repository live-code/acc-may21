import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { Meteo } from '../../model/meteo';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-weather',
  template: `
    <div *ngIf="data">
      <h1>{{city}} Meteo</h1>
      <h2>{{data?.main.temp}} °</h2>
      <img [src]="'http://openweathermap.org/img/w/' + data?.weather[0].icon +'.png'" alt="">
      <hr>
      <!--<pre>{{data | json}}</pre>-->
    </div>
  `,
})
export class WeatherComponent implements OnChanges {
  @Input() city: string;
  data: Meteo;

  constructor(private http: HttpClient) {  }

  ngOnChanges(): void {
    if (this.city) {
      this.http.get<Meteo>(`http://api.openweathermap.org/data/2.5/weather?q=${this.city}&units=metric&APPID=eb03b1f5e5afb5f4a4edb40c1ef2f534`)
        .subscribe(res => {
          this.data = res;
        });
    }
  }
}

// ----------------------------->
