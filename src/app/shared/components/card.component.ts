import { Component, Input, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-card',
  template: `
    <div class="card">
      <div 
        class="card-header bg-dark text-white" 
        *ngIf="title"
        (click)="open = !open"
      >
        {{title}}
        
        <div class="pull-right">
          <i 
            *ngIf="icon" [class]="icon"
            (click)="iconClick.emit()"
          ></i>
        </div>
      </div>
      <div class="card-body" *ngIf="open">
        <ng-content></ng-content>
      </div>
      <div class="card-footer" *ngIf="footerLabel">
        <button (click)="clickHandler()">{{footerLabel}}</button>
      </div>
    </div>
  `,
  styles: [
  ]
})
export class CardComponent {
  @Input() title: string;
  @Input() icon: string;
  @Input() footerLabel: string;
  @Output() iconClick = new EventEmitter();
  @Output() footerButtonClick = new EventEmitter();
  open = true;

  clickHandler(): void {
    this.footerButtonClick.emit()
  }
}
